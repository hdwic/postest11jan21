const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
app.use(express.json())
const { todos,users } = require('./models')

// seed BD
/*
model todos
{
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    due_at: DataTypes.DATEONLY,
    user_id: DataTypes.INTEGER
  }

model  users
{
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING


*/

app.get('/todos', (req, res) => {
    todos.findAll().then(todo => {
      res.status(200).json({
        status: true,
        message: 'To Do Lists!',
        data: { todo }
      })
    })
  })

app.get('/users', (req, res) => {
    users.findAll().then(user => {
      res.status(200).json({
        status: true,
        message: 'User Lists!',
        data: { user }
      })
    })
 })

 
 app.post('/users', (req, res) => {
    // console.log(users.findOne({ where: { email: req.body.email } }));
    const cekEmail = users.findOne({ where: { email: req.body.email } });
    if (cekEmail === null) {
        users.create({
      
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            
          }).then(user => {
            res.status(201).json({
              status: true,
              message: 'users created!',
              data: user 
            })
          })
      } else {
        res.status(403).json({
            message: 'users already exist, use other email', 
        })
    }
})


 
 app.post('/todos', (req, res) => {
    const cekUserId = users.findOne({ where: { user_id: req.body.user_id } });
    if (cekUserId=== null){
        res.status(403).json({
            message: 'users not found, can not create todos', 
        })
    } else {
        todos.create({
            name: req.body.name,
            description: req.body.description,
            due_at: req.body.due_at,
            user_id: req.body.user_id,
          }).then(todo => {
            res.status(201).json({
              status: true,
              message: 'todos created!',
              data: todo 
            })
          })
    }
})

/*
app.get('/users/:id', (req, res) => {    
  users.findByPk(req.params.id).then(user => {    
    res.status(200).json({    
      status: true,
      message: `Users with ID ${req.params.id}     retrieved!`,
      data: user
    })
  })
})

*/

// FIND AND UPDATE

// users
app.put('/users/:id', (req, res) => {
  // let { name,email,password } = req.body;

  users.findByPk(req.params.id).then(user => {
    user.update({
      name : req.body.name,
      email: req.body.email,
      password: req.body.password
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `users with ID ${req.params.id} updated!`,
        data: user 
      })
    })
  })

});

//todos
app.put('/todos/:id', (req, res) => {
  // let { name,email,password } = req.body;

  todos.findByPk(req.params.id).then(todo => {
    todo.update({
      
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id: req.body.user_id}, 
      {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `todos with ID ${req.params.id} updated!`,
        data: todo 
      })
    })
  })

});

//DELETE
//users
app.delete('/users/:id', (req, res) => {
  users.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

//todos
app.delete('/todos/:id', (req, res) => {
  todos.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

//   if (name||email||password !== null){
//     res.status(200).json({
      
//       req.params.id
//       users.update({
//         subject: params.firstName, body: params.lastName, status: params.status
//     },{
//         returning:true,
//         where: {id:id }                             
//     }).then(function(){
    
    
    
    
    
//     })
//   }
//   let updatedData = 
  
//   books[bookIndex-1].title = req.body.title;
//   books[bookIndex-1].author = req.body.author;
  
// })





app.listen(port, () => console.log(`Listening on port ${port}!`));