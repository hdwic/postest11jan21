
//Async version for PostgressExpress App

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
app.use(express.json())
const { todos,users } = require('./models')


app.get('/todos', async(req, res) => {
    const todo = await todos.findAll()
        res.status(200).json({
        status: true,
        message: 'To Do Lists!',
        data: { todo }
      })
  })

app.get('/users', async(req, res) => {
    const user = await users.findAll()
      res.status(200).json({
        status: true,
        message: 'User Lists!',
        data: { user }
      })
    })

 
 app.post('/users', async (req, res) => {
    // console.log(users.findOne({ where: { email: req.body.email } }));
    const cekEmail = await users.findOne({ where: { email: req.body.email } });
    if (cekEmail === null) {
        const user = await users.create({
      
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            
          });
        res.status(201).json({
            status: true,
            message: 'users created!',
            data: user 
            })
        
      } else {
        res.status(403).json({
            message: 'users already exist, use other email', 
        })
    
    }
 })

 
 app.post('/todos', async (req, res) => {
    const cekUserId = await users.findOne({ where: { user_id: req.body.user_id } });
    if (cekUserId=== null){
        res.status(403).json({
            message: 'users not found, can not create todos', 
        })
    } else {
        const todo = await todos.create({
            name: req.body.name,
            description: req.body.description,
            due_at: req.body.due_at,
            user_id: req.body.user_id,
        })
            res.status(201).json({
              status: true,
              message: 'todos created!',
              data: todo 
            })
    }
})


// users
app.put('/users/:id', async (req, res) => {
   const foundUser = await users.findByPk(req.params.id)
   const updatedUser = await foundUser.update({
        name : req.body.name,
        email: req.body.email,
        password: req.body.password
    }, {
        where: {
            id: req.params.id
        }
    })
    res.status(200).json({
        status: true,
        message: `users with ID ${req.params.id} updated!`,
        data: updatedUser 
    })
})
  
//todos
app.put('/todos/:id', async (req, res) => {
  // let { name,email,password } = req.body;

  const todosKey = await todos.findByPk(req.params.id)
  const todo = await todosKey.update({
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id: req.body.user_id}, 
      {
        where: {
            id: req.params.id
        }
      })
      res.status(200).json({
        status: true,
        message: `todos with ID ${req.params.id} updated!`,
        data: todo 
      })
})

//DELETE
//users
app.delete('/users/:id', async (req, res) => {
    await users.destroy({
        where: {
            id: req.params.id
        }
    })
    res.status(204).end()
})

//todos
app.delete('/todos/:id', async (req, res) => {
    await todos.destroy({
        where: {
            id: req.params.id
        }
    })
    res.status(204).end()
  })


  
app.listen(port, () => console.log(`Listening on port ${port}!`));