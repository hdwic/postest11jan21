
//CALLBACK version for PostgressExpress App


const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
app.use(express.json())
const { todos,users } = require('./models')


//findAll
//todos
const todosFindAllCb = (callback) => {
    todos.findAll().then(todo => {
        return callback(todo, null);    
    }).catch(err =>{
        return callback(null, err);
    })
}

//users
const usersFindAllCb = (callback) => {
    users.findAll().then(user => {
        return callback(user, null);    
    }).catch(err =>{
        return callback(null, err);
    })
}

//usercreate with callback
const usersCreateCB = (name,email,password,callback) => {
    users.create({
      
        name: name,
        email: email,
        password: password,
        
      }).then(user => {
        return (callback(user, null));
      }).catch(err =>{
          return(callback(null,err));
      })
}

const todosCreateCB = (name,description,due_at,user_id,callback) => {
    todos.create({
      
        name: name,
        description: description,
        due_at: due_at,
        user_id: user_id,

      }).then(todo => {
        return (callback(todo, null));
      }).catch(err =>{
          return(callback(null,err));
      })
}


const usersUpdateCB = (id, name, email, password, callback) => {
    users.findByPk(id).then(user => {
        user.update({
          name : name,
          email: email,
          password: password
        }, {
          where: {
            id: id
          }
        }).then((user,err)=>{
            return (callback(user,null))
        }).catch(err=>{
            return(callback(null,err))
        })
    })
}    


const todosUpdateCB = (id, name, description, due_at, user_id, callback) => {
    todos.findByPk(id).then(todo => {
        todo.update({
          name : name,
          description: description,
          due_at: due_at,
          user_id: user_id
        }, {
          where: {
            id: id
          }
        }).then((todo,err)=>{
            return (callback(todo,null))
        }).catch(err=>{
            return(callback(null,err))
        })
    })
}

const usersDeleteCB = (id, callback) =>{
    users.destroy({
        where: {
            id: id
        }
    }).then((user,err)=>{
        return (callback(user, null))
    }).catch (err => {
        return(callback(null, err))
    })
}


const todosDeleteCB = (id, callback) =>{
    todos.destroy({
        where: {
            id: id
        }
    }).then((todo,err)=>{
        return (callback(todo, null))
    }).catch (err => {
        return(callback(null, err))
    })
}



app.get('/todos', (req, res) => {
    todosFindAllCb((todo,err) => {
        if (!err) {
            res.status(200).json({
                status: true,
                message: 'To Do Lists!',
                data: { todo }
              })      
        } else {
            res.status(404).json({
                message: 'Data todo not found!',
            })
        }
    })
})        

app.get('/users', (req, res) => {
    usersFindAllCb((user,err) => {
        if (!err) {
            res.status(200).json({
                status: true,
                message: 'User Lists!',
                data: { user }
              })      
        } else {
            res.status(404).json({
                message: 'User not found!',
            })
        }
    })
})    

app.post('/users', (req, res) => {
    // console.log(users.findOne({ where: { email: req.body.email } }));
    const cekEmail = users.findOne({ where: { email: req.body.email } })
    console.log(cekEmail)
    if (cekEmail === null) {
        usersCreateCB(req.body.name,req.body.email,req.body.password, (user, err) => {
            if(!err){
                res.status(201).json({
                    status: true,
                    message: 'users created!',
                    data: user 
                  })    
            } else {
                res.status(404).json({
                    status : false,
                    message: 'Error!!! can not create User!',
                })
            }    
        })      
        } else {
        res.status(403).json({
            message: 'users already exist, use other email', 
        })
    }
})


 
 app.post('/todos', (req, res) => {
    const cekUserId = users.findOne({ where: { user_id: req.body.user_id } });
    if (cekUserId=== null){
        res.status(403).json({
            status: false,
            message: 'users not found, can not create todos', 
        })
    } else {
        todosCreateCB(req.body.name,req.body.description,req.body.due_at, req.body.user_id, (todo, err) => {
            if(!err){
                res.status(201).json({
                    status: true,
                    message: 'todos created!',
                    data: todo 
                  })    
            } else {
                res.status(404).json({
                    status : false,
                    message: 'Error!!! can not create todos!',
                })
            }    
        })
    }
})

// FIND AND UPDATE

// users
app.put('/users/:id', (req, res) => {
    usersUpdateCB(req.params.id, req.body.name, req.body.email, req.body.password, (user,err)=>{
        if(!err){
            res.status(200).json({
                status: true,
                message: `users with ID ${req.params.id} updated!`,
                data: user 
            })      
        } else {
            res.status(404).json({
                status : false,
                message: 'Error!!! can not update users!',
            })
        }
    })
});


//todos
app.put('/todos/:id', (req, res) => {
    todosUpdateCB(req.params.id, req.body.name, req.body.description, req.body.due_at, req.body.user_id, (todo,err) =>{
        if(!err){
            res.status(200).json({
                status: true,
                message: `todos with ID ${req.params.id} updated!`,
                data: todo 
              })
        } else {
            res.status(404).json({
                status : false,
                message: 'Error!!! can not update todos!',
            })
        }
    })
  })


//DELETE
//users
app.delete('/users/:id', (req, res) => {
    usersDeleteCB(req.params.id, (user, err) => {
        if(!err){
            res.status(204).end()
        } else {
            res.status(404).json({
                status : false,
                message: `can not delete user with ID : ${req.params.id} `
            })
        }
    })
})

//todos
app.delete('/todos/:id', (req, res) => {
    todosDeleteCB(req.params.id, (todo, err) =>{
        if(!err){
            res.status(204).end()
        } else {
            res.status(404).json({
                status : false,
                message: `can not delete todo with ID : ${req.params.id} `
            })
        }
    })
})

app.listen(port, () => console.log(`Listening on port ${port}!`));